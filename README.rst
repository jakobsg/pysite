Description
===========

Pysite is a simple framework for developing web applications. Pysite delivers very little automation making it up to
the developer to create the magic. Pysite utilizes Jinja2 for templating and Qt's TS format for localization. TS files
have been chosen as format so that Qt's Linuguist tool can be used for translations.
