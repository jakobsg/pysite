PySite Configuration
********************

All PySite projects must have a *conf.py* in the base directory.

Writing a PySite conf.py
========================

Access configuration from subhandlers
=====================================

.. toctree::
   :maxdepth: 1
