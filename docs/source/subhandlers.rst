Subhandlers
***********

Subhandlers are normal Python modules that implements at least one of the function based entry-point signitures::

	def init ( template_info , response_headers , environ,translate , req_data ):
		...
		
	def redirect ( template_info , cookies,environ ):
		...

or a subhandler class::
	
	class MySubhandler(pysite.subhandlers.BaseHandler):
		
		def __init__( self, *args, **kw ):
			super(MySubhandler, self).__init__(self, *args, **kw)
			...
		
		def init(self):
			self.template_info['remote_addr'] = self.environ.get('REMOTE_ADDR')
			self.template_info['say_hello'] = self.translate('Hello')

Function based entry-points
===========================
Subhandlers with unction based subhandlers based on module scope functions

Initializer
-----------

Redirecter
----------

Subhandler class
================

init

Return based functions or generators
====================================

Language resources
==================

The translate function
----------------------

.. toctree::
   :maxdepth: 1
