PySite Project Structure
************************

This page discusses how PySite interprets the project structure.

Basic flow of PySite projects
=============================

PySite is a plugable web-site framework. Pages are driven by Jinja2 templates supported by subhandlers that are normal python scripts with a function or class-based entry-point.

Pages are path controlled in a very simple manner. If a user accesses "/main" relative the project's root path, PySite will by default render the templates/main.jinja template. But
before rendering, it will check if there is a corresponding subhandler: subhandlers/main.py. If such a subhandler exists and there is an initialization entry-point, this will be called
first. The initializer recieves all the necessary input to interpret the request, and the handles to pass data on to the jinja2 template system.

.. _special-file-type:

Special file types
------------------
PySite defines a standard list of file types (identified by file-extension) which are handled as raw downloads. The mime-type of the download can differ depending on the specific file type.
Examples:

* js
  
  * Content-Type: application/javascript
  * Content-Encoding: gzip
 
* html
  
  * Content-Type: text/html
  * Content-Encoding: gzip
 
* png
  
  * Content-Type: image/png
 

Project tree
============

* site

  * templates
  
    * main.jinja
    * login.jinja
    * ...
    
  * subhandlers
  
    * main.py
    * login.py
    * ...
    
  * static
  
    * js
    
      * script-1.js
      * script-2.js
      * ...
    
    * css
    
      * bluestyle.css
      * graystyle.css
      * ...

    * images
    
      * topborder.png
      * leftborder.png
      * ...

    * ...
    
  * conf.py

Subhandlers
-----------
Subhandlers deliver the logical layer of PySite. Everytime PySite is accessed and the target is not a :ref:`special-file-type` the framework will attempt to load a subhandler that corresponds to request path. If the subhandler exists and an entry-point is identified, it will be called. For example let us say that the end-user accessed "/main" and the main.py subhandler looks like this::

	import time
	
	def init(template_info,response_headers,environ,translate,req_data):
		template_info['cur_ctime'] = time.time()

With the initializer code above the current unix timestamp will be accessible via {{ cur_ctime }} in main.jinja.

Templates
---------
.. _Jinja2 documentation: http://jinja.pocoo.org/docs/dev/
The template system uses Jinja2 for rendering - please refer to the `Jinja2 documentation`_

.. toctree::
   :maxdepth: 1
