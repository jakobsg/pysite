.. PySite documentation master file, created by
   sphinx-quickstart on Thu Feb  3 00:18:00 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PySite's documentation!
==================================

Introduction
------------

Pysite is a simple framework for developing web applications. Pysite delivers very little automation making it
up to the developer to create the magic.

Pysite utilizes Jinja2 for templating and Qt's TS format for localization. TS files have been chosen as format
so that Qt's Linuguist tool can be used for translations. PySite has translation support in jinja, python and
JavaScript.

.. image:: images/pysite.png

Quickstart
----------

PySite comes with a command-line tool (pysite-x.y - x,y mathing the python version). In the following this tool
will be referred to as the pysite-tool. The pysite-tool is used to manage your PySite based sites, ie. updating
language resources or test serve your site on a specified port using a built-in standalone WSGI web server.

Creating a PySite project
+++++++++++++++++++++++++
To create a new PySite use the pysite-tool with the "init" subcommand. In the following example we will create
the famous Hello world PySite project::

	pysite-2.7 init helloworld --site-title="Hello World"

Testing a PySite project
++++++++++++++++++++++++
Again you can use the pysite-tool to quickly test your site in a stand-alone manner::

	pysite-2.7 testserve helloworld


.. toctree::
   :maxdepth: 1

   site-structure.rst
   pysiteconfiguration.rst
   subhandlers.rst
   templates.rst
   httptool.rst
   localization.rst
   api.rst
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


