Localization
************

PySite's localization system is very much inspired from Qt. In fact Qt Linguist is the tool to use for translating PySite language
resources.

Localized strings in subhandlers
================================

Localized strings in Jinja2 templates
=====================================

Localized strings in JavaScript
===============================

The localization cycle
======================

Updating TS-files
-----------------

Translating localized strings
-----------------------------

Releasing language translations
-------------------------------

.. toctree::
   :maxdepth: 1
