Changes
=======
New in 0.3.2
------------
- Subhandler classes redirect method received the cookies list reference.
- Fixed bug with pysite-ctl init

New in 0.3.1
------------
- Fix to application/x-www-form-urlencoded detection

New in 0.3.0
------------
- Skip javascript files with bad syntax while lupdating

New in 0.2.9
------------
- Added redirect support for class based subhandlers

New in 0.2.8
------------
- Possibility to override the content-type
- Removed "RuntimeWarning: Parent module 'subhandlers' not found while handling absolute import"

New in 0.2.7
------------
- Bug fixed with cookie parser in pysite.tools.httptools.HTTPRequestData

New in 0.2.6
------------
- Added browser_lang in template_info

New in 0.2.5
------------
- Added MANIFEST.in

New in 0.2.4
------------
- Added first specialized subhandler class: DownloadHandler

New in 0.2.3
------------
- Added documentation disposition
- Added translation support for subhandler classes

New in 0.2.2
------------
- Documentation started
- Preliminary JavaScript translation support
- Added basepath to the PySiteConfiguration class

New in 0.1.8
------------
- Added more file types

New in 0.1.7
------------
- Further Python 3 support

New in 0.1.6
------------
- Preliminary support for Python 3

New in 0.1.5
------------
- Fixed compressed data bug

New in 0.1.4
------------
- Altered pyinit, PySiteApplication, PySiteConfiguration to handle apache mod_wsgi

New in 0.1.3
------------
- Altered pysite init script to copy from pysite's resource folder

New in 0.1.1
------------
- Added pysite script for site administration and testserving

New in 0.1.0
------------
- Initial distribution
