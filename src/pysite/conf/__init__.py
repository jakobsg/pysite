from pysite.exceptions.conf import *
import imp
from os.path import abspath,dirname,join,normpath

class PySiteConfiguration(object):
	
	def __init__(self,basedir,sitename=None,sitetitle=None,logfile=None):
		"""
		Constructor takes the basedir of a PySite as argument. basedir must
		be a PySite root directory containing your subhandlers and templates
		directories. It is expected that basedir is either an absolute path or
		a path relative to the current working directory. Optional parameters are
		sitename which is the actial name of the PySite, sitetitle which is
		the displayed name or title of the site and logfile can be passed if
		you want an alternate destination for the site's log file (default is
		<basedir>/<sitename>.log)
		
		:param basedir: The PySite root directory.
		:param sitename: The name of your site.
		:param sitetitle: The displayed title of your site.
		:param logfile: Alternative destination for the site's log file.
		:type basedir: unicode/str
		:type sitename: unicode/str
		:type sitetitle: unicode/str
		:type logfile: unicode/str
		"""
		self.basedir = basedir
		self.basedir_a = abspath(normpath(basedir))
		if sitename:
			self.sitename = sitename
			
		if not getattr(self,'sitename',None):
			raise ConfException('Mandatory configuration member "sitename" is missing')
		
		if sitetitle:
			self.sitetitle = sitetitle
		elif not getattr(self,'sitetitle',None):
			self.sitetitle = self.sitename

		if logfile:
			self.logfile = logfile
		elif not getattr(self,'logfile',None):
			self.logfile = join(self.basedir,'%s.log' % self.sitename)

	def basedir_abspath(self,*args):
		"""
		Get the absolute path to the site base directory
		
		:return: Return the absolute path to your site's root or a site subdir if arguments are passed.
		:rtype: unicode/str
		"""
		return join(*((self.basedir_a,) + args))

global_conf = None

def getConfiguration(basedir=None):
	"""
	Instantiate the PySiteConfiguration or descendent of the PySite located at basedir
	if basedir is not given it will eventually be interpretted as the current working
	directory.
	The basedir is expected to contain the PySite configuration file (conf.py)
	
	:param basedir: The PySite root directory.
	:type basedir: unicode/str
	:return: Returns the PySiteConfiguration object for the site located in basedir.
	:rtype: unicode/str
	"""
	global global_conf
	if not global_conf:
		basedir = basedir if basedir is not None else ''
		confmod = imp.load_source('conf',join(basedir,'conf.py'))
		global_conf = confmod.siteconf(basedir)
	return global_conf

get_conf = getConfiguration