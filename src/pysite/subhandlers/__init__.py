#!/usr/bin/python
# -*- coding: utf-8 -*-

class BaseHandler(object):
	"""
	Base class for your sub-handlers.

	Implement init(self) or redirect(self), in your derived class, to control
	how the request is handled.

	Use init() to do your own preparation before the jinja template is rendered.
	
	Use redirect() to abort rendering the requested page.

		Typically, you may want to add local scope variables to the template_info dict.

	Attributes:
		template_info (dict): local scope variables for your jinja template.
		response_headers (list): http response headers, you can add to.
		environ (dict): Your CGI system-environment variables.
		translate (function): your access to the translation system.
		req_data (class pysite.tools.httptools.HTTPRequestData): The request we are processing.

	You can access these in your init() and redirect().

	abstract method init(self):
	Args:
		no args - all args are class data members.

	abstract method redirect(self, cookies):
	Args:
		cookies(dict): A place where you can add cookies you want to transfer to new page. 
	Return value:
		if you return a URL, the server will 302-redirect the request there.
	"""
	def __init__(self,*args,**kw):
		self.template_info = args[0]
		self.response_headers = args[1]
		self.environ = args[2]
		self.translate = args[3]
		self.req_data = args[4]  
